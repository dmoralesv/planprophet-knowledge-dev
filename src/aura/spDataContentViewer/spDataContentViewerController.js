({
    doInit : function(component, event, helper) {
        console.log('spDataContentViewer v.1.1');
        component.set("v.loaded", false);
    },
    onDataChange : function(component, event, helper) {
        event.stopPropagation();
        component.set("v.loaded", true);
        
        var itemName = event.getParam('value');
     
        console.log("Data Content Viewer -- onDataChange -- "+ itemName);
        var itemSplit = itemName.split('-');
        if(itemSplit.length < 2){         
           return;
        }          
       
        var categNameOrArtId = itemSplit[0]; //or articleId
        var categGroup = itemSplit[itemSplit.length - 1];//ImAnArticle when is an article

        var contentType = 'CategoryGroup';
        if(categGroup == 'ImAnArticle')
          contentType = 'Article';
        else{
           if(itemSplit.length > 2)
             contentType = 'DataCategory'; 
         }      
            
        if(contentType == 'Article'){
           var knowledeApiName = component.get("v.knowledgeOjectApiName");
           var bodyApiName = component.get("v.articleBodyApiName");
           helper.loadArticleContent(component, categNameOrArtId, knowledeApiName, bodyApiName ); 
        }
        else if(contentType == 'CategoryGroup'){
           helper.loadContent(component, categGroup )
        }
        else if(contentType == 'DataCategory'){
          helper.loadDataCategoryContent(component, categGroup, categNameOrArtId);
        }
        else{
           component.set("v.loaded", false);
          return;
        }
         component.set("v.loaded", false);
       
    },
    
    onElementClick: function(component, event, helper){
      /*For events fired by standard HTML elements, you can use event.currentTarget and event.target. 
       *For events fired by base Lightning components, use event.getSource() instead. */
      var elementId = event.target.id;
      console.log('Element Id ' + elementId);
  
       var elementSelectedEvt = $A.get("e.c:spElementSelectedEvent"); 
       elementSelectedEvt.setParam("elementId", elementId);
       elementSelectedEvt.fire();  
    }
  

})