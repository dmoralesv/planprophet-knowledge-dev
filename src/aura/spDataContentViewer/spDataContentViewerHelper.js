({  
   loadContent : function(component, categoryGroup) {
      
        var params = {
            categoryGroup: categoryGroup	
        };
        this._invoke(
            component,
            'c.getContentForCategoryGroup',
            params,
            function(result) {
              component.set("v.content", result)				               
            },
            function(error){
            console.log('Error ' + error); 	
            }
        );
    },
    
    loadDataCategoryContent : function(component, categoryGroup, dataCateg) {
      
        var params = {
            categoryGroup: categoryGroup,
            dataCategory : 	dataCateg
        };
        this._invoke(
            component,
            'c.getContentForDataCategory',
            params,
            function(result) {
               component.set("v.content", result)				               
            },
            function(error){
            console.log('Error ' + error); 	
            }
        );
    },
    loadArticleContent : function(component, articleId, knowledgeApiName, bodyApiName) {
      
        var params = {
            articleId: articleId,
            knowledgeArticleName : 	knowledgeApiName,
            bodyApiName : bodyApiName
        };
        this._invoke(
            component,
            'c.getContentForArticle',
            params,
            function(result) {
               component.set("v.content", result)				               
            },
            function(error){
            console.log('Error ' + error); 	
            }
        );
    },   

    _invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":				
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}	
})