({
    doInit : function(component, event, helper) {
        console.log("Init Knowledge Viewer v.3");
        helper.loadKnowledgeData(component, helper);    
        // helper.loadCommunityBase(component);             
     },    

     /* when select element in the tree */  
     onTreeSelect : function(component, event, helper){
        event.preventDefault();
        console.log('Item Selected Name ' + event.getParam('name'));
        helper.hideWarning(component);
        var itemName = event.getParam('name');    
        
        if(helper.requireReload(component, itemName)){
           helper.changeArticleId(component, itemName);
           component.set("v.selectedItem", itemName);  
        }
        else{
            helper.reloadBreadCrumbs(component, itemName);
            component.set("v.selectedItem", itemName);  
        }          
     },

     onBreadCrumbSelect: function (component, event, helper) {
      helper.hideWarning(component);
      var name = event.getSource().get('v.name');
      console.log('Breadcrumb Name ' + name);
      helper.updateBreadCrumbsList(component, name);     
     },
    
    /** when change element selected in tree or in the content child component */
    onDataChange: function(component, event, helper){  
   
      var itemName = event.getParam('value');
      console.log('Data Category Viewer--- onChange--- ' + itemName);   
      helper.reloadBreadCrumbs(component, itemName);   

      event.stopPropagation();
  },

  onCloseWarning: function(component, event, helper){  
     helper.hideWarning(component);
  },

  onArticleIdChange: function(component, event, helper){
      helper.reloadArticlePage(component);
      event.stopPropagation();
  },

  /* Event throw by child component */
  onElementSelected: function(component, event, helper){
       console.log("1. RECEIVE EVENT---------------");
       helper.hideWarning(component);
       var elementId = event.getParam('elementId');
       if(helper.requireReload(component, elementId)){
         helper.changeArticleId(component, elementId);
         component.set("v.selectedItem", elementId);   
       }
      else {
          helper.reloadBreadCrumbs(component, elementId);  
          component.set("v.selectedItem", elementId);       
       }        
  },
  /* button to show or hide tree navigation */
  onExpandTree: function(component){
      var isExpanded = component.get("v.treeExpanded");
      component.set("v.treeExpanded", !isExpanded);
  }
})