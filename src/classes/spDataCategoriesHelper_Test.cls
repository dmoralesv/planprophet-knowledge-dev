/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 10-23-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   10-23-2020   dmorales   Initial Version
**/
@isTest
private class spDataCategoriesHelper_Test {

    @isTest
    static void test_getCategoryTree() {
        List<spDataCategoriesHelper.ItemWrapper> items = spDataCategoriesHelper.getCategoryTreeWithArticles('');
        System.assert(items.size() > 0, 'There are not elements in Item List');
    }

    @isTest
    static void test_getCategoryTreeWithDataCategory() {        
        List<String> objName = new List<String>{spDataCategoriesConstants.ARTICLE_SOBJECT_NAME};
        List<Schema.DescribeDataCategoryGroupResult> results = Schema.describeDataCategoryGroups(objName);  
        if(results != null && results.size() > 0){
            Schema.DescribeDataCategoryGroupResult firstGroup = results[0];    
            List<spDataCategoriesHelper.ItemWrapper> items = spDataCategoriesHelper.getCategoryTreeWithArticles(firstGroup.getName());
            System.assert(items.size() > 0, 'There are not elements in Item List');
        }                  
    }  
    
    @isTest
    static void test_getTopDataCategory() {        
        List<String> objName = new List<String>{spDataCategoriesConstants.ARTICLE_SOBJECT_NAME};
        List<Schema.DescribeDataCategoryGroupResult> results = Schema.describeDataCategoryGroups(objName);  
        if(results != null && results.size() > 0){
            Schema.DescribeDataCategoryGroupResult firstGroup = results[0];  
                       
            DataCategory topCat = spDataCategoriesHelper.getTopDataCategory(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, firstGroup.getName());
            
        }                  
    }
}