/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 10-23-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   10-06-2020   dmorales   Initial Version
**/
global class spDataCategoriesConstants {
    global static final String ARTICLE_SOBJECT_NAME = 'KnowledgeArticleVersion';
    /* For Test Class, Update before deploying to specific Org */
    global static final String KNOWLEDGE_NAME = 'Knowledge__kav';
    global static final String ARTICLE_BODY_NAME = 'Body__c';
    global static final String DATA_CATEGORY_NAME = 'Content';
}