/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 10-13-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   10-07-2020   dmorales   Initial Version
**/
public without sharing class spContentViewerCtrl {
     public static String parentOutside = '';
     @AuraEnabled
     public static ContentGroupWrapper getContentForCategoryGroup(String categoryGroup){
        
         List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult = 
           spDataCategoriesHelper.getDescribeCategoryStructureResult(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, categoryGroup);
         if (describeCategoryStructureResult.isEmpty()) {
            return null;
         }
         //else
         DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0]; //Category Group element
         //Get the top level categories
         List<DataCategory> topLevelCategories = singleResult.getTopCategories();        
         DataCategory topCat = topLevelCategories[0];
         String topCategItemName = topCat.getName()+'-'+singleResult.getName();
        
        ContentGroupWrapper displayContent = new ContentGroupWrapper();
        displayContent.title = singleResult.getLabel();
        displayContent.name = topCategItemName;
        displayContent.description = singleResult.getDescription();

       
        for(DataCategory childCat : topCat.getChildCategories()){
            displayContent.dataCategoriesElements.add(new ContentElementWrapper(childCat.getLabel(), childCat.getName()+'-'+topCategItemName, ''));
        }

        getArticlesOfCategory(displayContent, categoryGroup, topCat.getName(), topCategItemName) ;
       
        return displayContent;
     }
    
     @AuraEnabled
     public static ContentGroupWrapper getContentForDataCategory(String categoryGroup, String dataCategory){
      
        DataCategory topCat = spDataCategoriesHelper.getTopDataCategory(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, categoryGroup);
       
        ContentGroupWrapper displayContent = new ContentGroupWrapper();
    
        String parentName = topCat.getName() + '-' + categoryGroup; 
      
        getChildCategories(dataCategory, topCat.getChildCategories(), displayContent, parentName);        
       
        getArticlesOfCategory(displayContent, categoryGroup, dataCategory, parentOutside) ;       
      
        return displayContent;
     }

     @AuraEnabled
     public static ContentGroupWrapper getContentForArticle(String articleId, String knowledgeArticleName, String bodyApiName){      
       
        ContentGroupWrapper displayContent = new ContentGroupWrapper();    
        
        String kwnowledgeName = String.escapeSingleQuotes(knowledgeArticleName);
        String artId =  String.escapeSingleQuotes(articleId);
        String query = ' SELECT Id, KnowledgeArticleId, Title, Body__c, Summary, UrlName, PublishStatus ' +
                       ' FROM ' + knowledgeArticleName +
                       ' WHERE Id = \'' + articleId + '\' ';
        
        List<sObject> articleList =  Database.query(query);
     
        if(articleList == null || articleList.size() == 0)
           return displayContent;
           
        //else
           displayContent.title = (String)articleList[0].get('Title');
           displayContent.name = (String)articleList[0].get('UrlName');
           displayContent.body = (String)articleList[0].get(bodyApiName);

           return displayContent;          

     }


     public static void getChildCategories(String dataCategory, List<DataCategory> dataCategList, 
     ContentGroupWrapper contentWrap, String parentName){
     
        for(DataCategory childCat : dataCategList){
         
           List<DataCategory> childList = childCat.getChildCategories();
           if(childCat.getName() == dataCategory){
              contentWrap.name = childCat.getName();
              contentWrap.title = childCat.getLabel();
              //contentWrap.description = 'This is a Data Category';     
              parentOutside =  dataCategory + '-' + parentName;     
              for(DataCategory childFound : childList){             
                 parentName = childFound.getName()+ '-'+ dataCategory + '-' + parentName;               
                 contentWrap.dataCategoriesElements.add(new ContentElementWrapper(childFound.getLabel(), parentName, ''));
              } 
              break;             
           }
           String parentName1 = childCat.getName()+ '-'+ parentName;        
           getChildCategories(dataCategory, childList, contentWrap, parentName1);
        }
     }

     public static void getArticlesOfCategory(ContentGroupWrapper contentWrap, String categoryGroup, String dataCategory, String parentName ){

             List<KnowledgeArticleVersion> articleList = spDataCategoriesHelper.getArticlesUrlByCateg(categoryGroup,dataCategory);
               for(KnowledgeArticleVersion article : articleList){
                  contentWrap.articlesElements.add(new ContentElementWrapper(article.Title, article.Id+'-'+parentName+'-ImAnArticle', article.Summary));
               }  
      }


     public class ContentGroupWrapper {
         @AuraEnabled
         public String title {set;get;}
         @AuraEnabled
         public String name {set;get;}
         @AuraEnabled
         public String description {set;get;}
         @AuraEnabled      
         public String body {set;get;}       
         @AuraEnabled
         public List<ContentElementWrapper> dataCategoriesElements {set;get;} 
         @AuraEnabled
         public List<ContentElementWrapper> articlesElements {set;get;}
         public ContentGroupWrapper(){
            this.dataCategoriesElements = new List<ContentElementWrapper>();
            this.articlesElements = new List<ContentElementWrapper>();
         }
     }

     public class ContentElementWrapper{
         @AuraEnabled
         public String label {set;get;}
         @AuraEnabled
         public String name {set;get;}
         @AuraEnabled
         public String articleSummary {set;get;}
         public ContentElementWrapper(String label, String name, String articleSummary){
           this.label = label;
           this.name = name;
           this.articleSummary = articleSummary;
         }
     }

}