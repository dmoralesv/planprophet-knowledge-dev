/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 10-23-2020
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   10-23-2020   dmorales   Initial Version
 **/
@isTest
private class spDataCategoryViewerCtrl_Test {
    @isTest
    static void test_getCategoryGroupTree() {
        List<spDataCategoriesHelper.ItemWrapper> listI = spDataCategoryViewerCtrl.getCategoryGroupTree('');
        System.assert (listI.size() > 0, 'No list item');
    }

    @isTest
    static void test_getArticlesUrlByCateg() {

        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult =
            spDataCategoriesHelper.getDescribeCategoryStructureResult(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME,
                                                                      spDataCategoriesConstants.DATA_CATEGORY_NAME);
        if (!describeCategoryStructureResult.isEmpty()) {
            DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0]; //Category Group element
            //Get the top level categories
            List<DataCategory> topLevelCategories = singleResult.getTopCategories();
            DataCategory topCat = topLevelCategories[0];

            List<KnowledgeArticleVersion> listArticles =  spDataCategoryViewerCtrl.getArticlesUrlByCateg(spDataCategoriesConstants.DATA_CATEGORY_NAME, topCat.getName());
            System.assert(listArticles != null, 'No Articles Under ' + spDataCategoriesConstants.DATA_CATEGORY_NAME + ' ' + topCat.getName());
        }
    }

}